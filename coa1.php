<!-- database connetion -->
<?php
	include_once('connection.php');
	session_start();
	if ( isset( $_SESSION['id'] ) ) {
	//select data
	$sql = "SELECT * FROM acc_coa1 ORDER BY id DESC";
	$result = $conn->query($sql);

	
//getting fields data
	
	


	if(isset($_POST['submit'])){
		$acc_code =	$_POST['acc_code'];
		$acc_desc =	$_POST['acc_desc'];

		//injections
		$acc_code = stripslashes($acc_code);
		$acc_desc = stripslashes($acc_desc);

		$acc_code = strip_tags($acc_code);
		$acc_desc = strip_tags($acc_desc);

		
     	 if (!$acc_desc) 
	 	$errorMsg = $errorMsg.'<span style="color:#ff0000">Your description is required<br />';

	 	$acc_code = mysqli_real_escape_string($conn,$acc_code);
		$acc_desc = mysqli_real_escape_string($conn,$acc_desc);


//insert data 
			$query = "INSERT into acc_coa1 (acc_code,acc_desc) values ('$acc_code','$acc_desc')";
	
				if ($conn->query($query) === TRUE) {
    		echo "New record created successfully";
    		 header( 'location:coa1.php');
			}
			 else {
    		echo "Error: " . $query . "<br>" . $conn->error;
			}

	
	
	}
	
	



?>



<!DOCTYPE html>
<html>
	<head>
		<title>Control Accounts</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- stylesheet -->
	<style type="text/css">
		<style type="text/css">
			
			label{
				font-size: 20px;
				font-weight: bold;
			}
			form{
				text-align: center;
				margin-top: 15%;
			}
			.btn{
				float: left;
				padding: 5px 19px;
			}
			.dtl{
				float: right;
			}
		</style>
	</head>
	<body>
		<?php include_once('navbar.php') ?> 
		<div class="container">
							<div class="dtl">	<a href="coa1-detail.php"><button class="btn btn-danger">View Detail</button></a></div>
							<br><br>
			<center><h2>Control Accounts</h2></center>
			<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<form class="form-horizontal" method="post" enctype="multipart/formdata">

					<!-- fetching data -->
					<?php $row = $result->fetch_assoc();?>


					<div class="form-group-">
						<div class="row">
					<label class="control-label col-sm-3 " for="code"> Acc. Code  </label>
					<div class="col-sm-9">
							<input readonly="readonly" class="form-control "  type="text" name="acc_code" required="required" 
								 value="<?php
								 	
									if ($result->num_rows > 0 and $result->num_rows < 5 ) {	$sum = $row['acc_code']+2;	echo  "0".$sum;	}
									else if($result->num_rows >= 5)
										{ 
											
											

											echo @$sum = $row['acc_code']+2;
										} 

									else { echo "01"; }

								 ?>">
					</div>
				</div>
					</div>

					 &nbsp &nbsp &nbsp 

					 <div class="form-group-">
					 	<div class="row">
						<label class="control-label col-sm-3">Acc. Desc</label>
						<div class="col-sm-9">
							<input class="form-control" type="text" name="acc_desc" required="required">
						</div>
					</div>
					</div>
				</br>
				<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-9">
					<input class="btn" type="submit" name="submit" value="Save">
				</div>
			</div>

				</form>
			</div>
			<div class="col-sm-3"></div>
		</div>
		</div>
	</body>
</html>
<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>