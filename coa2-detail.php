<?php
//connetion to database
 include_once('connection.php');
 session_start();
 if ( isset( $_SESSION['id'] ) ) {
 //selecting data using left joint
$sql = "SELECT acc_2.coa1_acc_code,acc_2.coa1_acc_desc,acc_2.acc_code,acc_2.acc_desc as acc2_desc ,acc_1.acc_desc as acc1_desc FROM acc_coa2 as acc_2 LEFT JOIN acc_coa1 as acc_1 ON acc_1.id = acc_2.coa1_acc_desc order by acc_2.acc_code asc ";
$result = $conn->query($sql);
 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>COA2 detail</title>
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<style type="text/css">
		.table{
			width: 50%;	
		}
		.dtl{
				float: right;
			}
	</style>
 </head>
 <body>
 	<?php include_once('navbar.php') ?> 
	<div class="container">
	<div class="dtl">	<a href="coa2.php"><button class="btn btn-danger"><-- GO BACK</button></a></div>
 <center><br><br>
  <table class="table table-bordered">
  	<thead class="thead "><h2>Sub Control Accounts Detail</h2></thead><br><br><br>
  	<tbody class="tbody">
  		<tr class="thead-dark">
  			<th>ID</th>
  			<th>COA1</th>
  			<th>COA2</th>
  		</tr>
  		<?php
			$i=1;
  			while($i<= $row = $result->fetch_assoc()){
		echo "<tr>";
		echo     "<td>". $i."</td>";
		echo	"<td>".$row['coa1_acc_code'].'-'.$row['acc1_desc']."</td>";
		echo	"<td>".$row['acc_code']." ".$row['acc2_desc']."</td>";
		$i++;
		?>	
		<?php 
	echo "</tr>";
}
  	?>
  	</tbody>
  	
  </table></center>
</div>
 </body>
 </html>
 <?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>