<!-- database connetion -->
<?php
	include_once('connection.php');
	session_start();
	if ( isset( $_SESSION['id'] ) ) {

	//select data
	$sql = "SELECT * FROM acc_coa";
	$result = $conn->query($sql);

	$sql4 = "SELECT DISTINCT vou_date FROM acc_vou_mst order by vou_date asc ";
	$result4 = $conn->query($sql4);
	while($row321 = $result4->fetch_assoc()){
		$date[] = $row321['vou_date'];
	}

	if(isset($_POST['submit'])){

		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$coa_code = $_POST['coa_code'];

		$sql1 = "SELECT * FROM acc_coa where acc_code = $coa_code";
		$result1 = $conn->query($sql1);
		$row3 = $result1->fetch_assoc();


		$select= "SELECT acc_vou_dtl.acc_code,acc_vou_dtl.vou_id,acc_vou_dtl.transaction_type,acc_vou_dtl.reference_no, acc_vou_dtl.remarks, acc_vou_dtl.dr, acc_vou_dtl.cr,  acc_vou_mst.vou_type_id, acc_vou_mst.vou_no, acc_vou_mst.vou_date FROM  acc_vou_dtl, acc_vou_mst where acc_vou_mst.id = acc_vou_dtl.vou_id and  acc_vou_dtl.acc_code = $coa_code and acc_vou_mst.vou_date >= '$from_date ' and acc_vou_mst.vou_date <= '$to_date ' order by acc_vou_mst.vou_date , acc_vou_mst.vou_no ";
		$result2 = $conn->query($select);

		
	}

?>



<!DOCTYPE html>
<html>
	<head>
		<title>General Ledger</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!-- stylesheet -->
	<style type="text/css">
		<style type="text/css">
			
			label{
				font-size: 20px;
				font-weight: bold;
			}
			form{
				text-align: center;
				margin-top: 15%;
			}
			.btn{
				float: left;
				padding: 5px 19px;
			}
			.select2-container .select2-selection--single {
			    
			    height: 38px;
			    display: block;
			    }
			 .col-sm-4{
			 	    padding-right: 4px !important;
			 }
			 .col-sm-2{
			 	padding-right: 0px !important;
			 }
			 .col-sm-10{
			 	padding-right: 4px !important;
			 }
		</style>
	</head>
	<body>
		<?php include_once('navbar.php') ?> 
		<br><br>
		<div class="container">
			<center><h2>General Ledger</h2></center>
			<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<form class="form-horizontal" method="post" enctype="multipart/formdata">

					<div class="form-group-">
						<div class="row">
					<label class="control-label col-sm-2 "> From Date  </label>
					<div class="col-sm-4">
						<input class="form-control" type="date" id="theDate" name="from_date">
						
					</div>


					<label class="control-label col-sm-2 "> To Date  </label>
						<div class="col-sm-4">
							<input class="form-control" type="date" id="t-Date" name="to_date">
							
					</div>
				</div>
					</div>

					 &nbsp &nbsp &nbsp 

					 <div class="form-group-">
					 	<div class="row">
						<label class="control-label col-sm-2">Account</label>
						<div class="col-sm-10">
							<select class="js-example-placeholder-single js-states form-control" required="required"   name="coa_code">
					  					<option></option>
					  					<?php

					  						while($row = $result->fetch_assoc()){

											echo	"<option value=".$row['acc_code'].">"  .$row['acc_desc']. "</option>";
										}
						
										?>
							</select>
						</div>
					</div>
					</div>
				</br>
				<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-10">
					<input type="submit" class="btn" name="submit" value="View Report">
				</div>
			</div>

				</form>
			</div>
			<div class="col-sm-3"></div>
		</div>
		</div>

		

		
	<div class="container">
	
 <center><br><br><br><br>
  <table class="table table-bordered">
  	<thead class="thead "><h2>General Ledger Detail</h2></thead><br>
  	<h6>From Date: <?php echo  @$from_date; ?> &emsp; &emsp;&emsp; To Date: <?php echo  @$to_date; ?></h6><br><br>
  	<h3><?php echo @$row3['acc_desc']; ?></h3>
  	<br><br>
  	<tbody class="tbody">
  		<tr class="thead-dark">
  			<th>ID</th>
  			<th>Voucher</th>
  			<th>Dated</th>
  			<th>Description</th>
  			<th>Transaction Type</th>
  			<th>Transaction #</th>
  			<th>Dr.Amount</th>
  			<th>Cr.Amount</th>
  			<th>Balance</th>
  		</tr>
  		<?php
  			if(isset($_POST['submit'])){
  				$Balance="";
  				$dr ="";
  				$cr ="";
  				$t_belence = "";
				$i=1;
		  			while($i<= $row2 = $result2->fetch_assoc()){
		  				$id=$row2['vou_type_id'];
		  				
		  				@$dr+=  $row2['dr'];
		  
		  				@$cr+=  $row2['cr'];
		  				
				 
				$sql4 = "SELECT * FROM acc_vou_type where id = $id ";
				$result4 = $conn->query($sql4);
				$row4 = $result4->fetch_assoc();


			echo "<tr>";
				echo     "<td>". $i."</td>";
				echo	"<td>".$row4['vou_abrv']."&nbsp&nbsp&nbsp  ".$row2['vou_type_id']."</td>";
				echo	"<td>".$row2['vou_date']."</td>";
				echo	"<td>".$row2['remarks']."</td>";
				echo	"<td>".$row2['transaction_type']."</td>";
				echo	"<td>".$row2['reference_no']."</td>";
				echo	"<td>".$row2['dr']."</td>";
				echo	"<td>".$row2['cr']."</td>";
				
				if($i==1){
					$Balance=$row2['dr']-$row2['cr'];
					if($Balance<0){
					$Balnce= $Balance*-1;
					echo	"<td> Cr.&nbsp&nbsp&nbsp".$Balnce."</td>";
					}
					elseif($Balance>0){
						echo	"<td>Dr.&nbsp&nbsp&nbsp".$Balance."</td>";
					}
					else{
						echo	"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".$Balance."</td>";
					}
				}
				else{
					$Balance=$Balance+$row2['dr']-$row2['cr'];
					
					if($Balance<0){
					$Balnce= $Balance*-1;
					echo	"<td> Cr.&nbsp&nbsp&nbsp".$Balnce."</td>";
					}
					elseif($Balance>0){
						echo	"<td>Dr.&nbsp&nbsp&nbsp".$Balance."</td>";
					}
					else{
						echo	"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".$Balance."</td>";
					}
				}
				
				
				
				
				$i++;
				

			
				?>
						
						
				<?php 
			echo "</tr>";
			}
			?>

			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>Total = <?php echo $dr; ?></td>
				<td>Total = <?php echo $cr; ?></td>
				<td>Total = 
					<?php @$t_belence= $dr - $cr;
						if($t_belence<0){
							$t_belence= $t_belence*-1;
							echo	" Cr.&nbsp".$t_belence;
							}
							elseif($Balance>0){
								echo	"Dr.&nbsp".$t_belence;
							}
							else{
								echo	$t_belence;
							}
						  	
					 ?></td>
			</tr>
			<?php 
		}
  		?>
  	</tbody>
  	
  </table></center>
</div>
<script type="text/javascript">
			$(".js-example-placeholder-single").select2({
				    placeholder: "Select Account",
				    allowClear: true
				});
			$(".js-example-placeholder-single1").select2({
				    placeholder: "f_date",
				    allowClear: true
				});
			$(".js-example-placeholder-single2").select2({
				    placeholder: "t_date",
				    allowClear: true
				});
			var date = new Date();

			var day = date.getDate();
			var month = date.getMonth() + 1;
			var year = date.getFullYear();

			if (month < 10) month = "0" + month;
			if (day < 10) day = "0" + day;

			var today = year + "-" + month + "-" + day;


			document.getElementById('theDate').value = today;
			document.getElementById('t-Date').value = today;
		</script>
	</body>
</html>
<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>