<!-- database connetion -->
<?php
	include_once('connection.php');
	session_start();
	if ( isset( $_SESSION['id'] ) ) {


	//getting fields data
	
	

	if(isset($_POST['submit'])){
		$vou_name =	$_POST['vou_name'];
		$vou_abrv =	$_POST['vou_abrv'];

			//do some injection cleaning
		$vou_name = stripslashes($vou_name);
		$vou_abrv = stripslashes($vou_abrv);
		
		
		$vou_name = strip_tags($vou_name);
		$vou_abrv = strip_tags($vou_abrv);
		
		
		$vou_name = mysqli_real_escape_string($conn,$vou_name);
		$vou_abrv = mysqli_real_escape_string($conn,$vou_abrv);
		

	
//insert data 
			$query = "INSERT into acc_vou_type (vou_name,vou_abrv) values ('$vou_name','$vou_abrv')";
	
				if ($conn->query($query) === TRUE) {
    		echo "New record created successfully";
    		header( 'location:voucher_type.php');
			}
			 else {
    		echo "Error: " . $query . "<br>" . $conn->error;
			}

	
	
	}
	
?>



<!DOCTYPE html>
<html>
	<head>
		<title>Voucher Type</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- stylesheet -->
	<style type="text/css">
		<style type="text/css">
			
			label{
				font-size: 20px;
				font-weight: bold;
			}
			form{
				text-align: center;
				margin-top: 15%;
			}
			.btn{
				float: left;
				padding: 5px 19px;
			}
			.dtl{
				float: right;
			}
			
		</style>
	</head>
	<body>
		<?php include_once('navbar.php') ?> 
		<div class="container">
							<div class="dtl">	<a href="voucher_type_detail.php"><button class="btn btn-danger">View Detail</button></a></div>
							<br><br>
			<center><h2>Voucher Type</h2></center>
			<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<form class="form-horizontal" method="post" enctype="multipart/formdata">

					<!-- fetching data -->
					

					<div class="form-group-">
						<div class="row">
					<label class="control-label col-sm-3 " for="code"> Voucher Name  </label>
					<div class="col-sm-9">
							<input class="form-control "  type="text" name="vou_name" required="required" >
					</div>
				</div>
					</div>

					 &nbsp &nbsp &nbsp 

					 <div class="form-group-">
					 	<div class="row">
						<label class="control-label col-sm-3">Voucher ABRV</label>
						<div class="col-sm-9">
							<input class="form-control" type="text" name="vou_abrv" required="required">
						</div>
					</div>
					</div>
				</br>
				<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-9">
					<input class="btn" type="submit" name="submit" value="Save">
				</div>
			</div>

				</form>
			</div>
			<div class="col-sm-3"></div>
		</div>
		</div>
	</body>
</html>
<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>
