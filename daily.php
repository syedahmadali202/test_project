<?php
include_once('connection.php');
session_start();
if ( isset( $_SESSION['id'] ) ) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>Daily Scroll</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<style type="text/css">
	.row{
		margin-top: 15%;
		font-size: 16px;
		font-weight: bold;
		margin-bottom: 5%;
	}
	button{
		float: right;
	}
	
	@media print {
	 
   .footer{
     
   position: relative;
   bottom:0;
	}
   #btn1{
   	display: none;
   }
   #btn{
   	display: none;
   }
	}
	.row1{
		margin: 0px !important;
	}
	
</style>
<script>
function myFunction() {
  window.print();

}
</script>
<body>
	<?php include_once('navbar.php'); ?>
	<br><br>
	<center><h2>Daily Scroll</h2></center>
	<div class="container" id="btn1">
		<div class="row row1">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<form method="post">
					<div class="row ">
						<div class="col-sm-2"><label class="label-control">Date: </label></div>
						<div class="col-sm-10"><input class="form-control" id="theDate" type="date" name="date"></div>
					</div>
					<div class="row">
						<div class="col-sm-2"></div>
						<div class="col-sm-10"><input class="btn"  type="submit" name="submit" value="View Report"></div>
					</div>
				</form>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</div>
	<div class="container">
		
		<?php if(isset($_POST['submit'])){ 
		$date= $_POST['date'];
		?>
		<button type="btn" onclick="myFunction()" id="btn" class="btn btn-danger" value="Print"> Print Report</button>
	</br></br>
		
		<table class="table table-bordered" id="content" >
			<tbody>
				<tr class="thead-dark" >
					<th>Voucher#</th>
					<th>COA</th>
					<th>Transaction Type / No</th>
					<th>Dr</th>
					<th>Cr</th>
					<th>Signature</th> 	
				</tr>

				<?php
				$select= " SELECT *  from acc_vou_mst WHERE  vou_date = '$date' AND (vou_type_id = 1 OR vou_type_id = 4 OR vou_type_id = 5 OR vou_type_id = 6 OR vou_type_id = 9) order by vou_type_id ";
				$result = $conn->query($select);
				$temp = 0;
				$i=0;
				$dr=0;
				$cr=0;
				while($row = $result->fetch_assoc()){
					$id=$row['id'];
					$select1 = "SELECT * from acc_vou_dtl where vou_id ='$id'";
					$result1 = $conn->query($select1);
					$vo_id = $row['vou_type_id'];
					$i++;
					if($temp != $row['vou_type_id'] && $i>1){
						echo '<tr>';
						echo '<td>-----</td>';
						echo '<td>-----</td>';
						echo '<td>-----</td>';
						echo '<td>-----</td>';
						echo '<td>-----</td>';
						echo '<td>-----</td>';
						echo '</tr>';
					}

					$temp = $row['vou_type_id'];
					while($row2 =$result1->fetch_assoc()){
					$vo_id = $row['vou_type_id'];
					$select2 = "select * from acc_vou_type where id = '$vo_id'";
					$result2 = $conn->query($select2);
					$row3 = $result2->fetch_assoc();
					
					echo '<tr>';
					
					echo '<td>'.$row3['vou_abrv'].' '.$row['vou_no'] .'</td>';

					$acc_code =$row2['acc_code'];
					$select3 = "select * from acc_coa where acc_code = '$acc_code'";
					$result3 = $conn->query($select3);
					$row4 = $result3->fetch_assoc();

					echo '<td>'. $row4['acc_desc']. '</td>';

					if($row2['transaction_type'] != ''){

				
					echo '<td>'.$row2["transaction_type"].' '.$row2["reference_no"] . '</td>';
					}
					else{
						echo '<td>---</td>';
					}
					echo '<td>'.$row2['dr'] . '</td>';
					echo '<td>'.$row2['cr'] . '</td>';
					echo '<td> </td>';
					echo '</tr>';
					@$dr+= $row2['dr'];
					@$cr+= $row2['cr'];
					}		
					
				}?>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<th>Total: <?php echo $dr; ?></th>
						<th>Total: <?php echo $cr; ?></th>
						<td></td>
					</tr>

			</tbody>
		</table>
		<div class="row footer">
			<div class="col-sm-4">Accountant</div>
			<div class="col-sm-4">Accounts Manager</div>
			<div class="col-sm-4">Chief Executive</div>
		</div>
		<?php } ?>
	</div>
</body>
</html>
<script type="text/javascript">
	var date = new Date();

			var day = date.getDate();
			var month = date.getMonth() + 1;
			var year = date.getFullYear();

			if (month < 10) month = "0" + month;
			if (day < 10) day = "0" + day;

			var today = year + "-" + month + "-" + day;


			document.getElementById('theDate').value = today;
</script>
<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>