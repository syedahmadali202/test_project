<?php
session_start();
if(empty($_SESSION['id'])){
	include_once('connection.php');
	if(isset($_POST['submit'])){
		$u_email = $_POST['email'];
		$u_password = $_POST['password'];

		$select= "SELECT * from signup where email = '$u_email' and password = '$u_password'";
		$result = $conn->query($select);
		$row = $result->fetch_assoc();

		if ($row) {
    		 $_SESSION['id']= $row['id'];
    		 header( 'Refresh:0; url=coa1.php');
			}
			 else {
			 	echo "<script type='text/javascript'>
					alert('login Failed')
				</script>
			";
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<style type="text/css">
		body{
			background-image: url('images/1.jpg');
		}
		.link1{
			color: black;
			font-weight: bold;
		}
		.btn: hover{
			opacity: 0.7;
		}
		.link2{
			float: right;
			color: black;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<br><br>
	<center><h3>Please Login</h3></center><br><br>
	<div class="container">
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<form method="post">
					<input class="form-control" required="required" type="text" name="email" placeholder="Email.."><br>
					<input class="form-control" required="required" type="password" name="password" placeholder="Password.."><br>
					<input class="btn btn-success btn-block" type="submit" name="submit" value="Login"><br>
					<a class="link1" href="signup.php">Register for an account?</a> <a class="link2" href="reset.php">Forgot password?</a>
				</form>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</div>
</body>
</html>
<?php 
}
else{
header('location:coa1.php');
}
?>