<?php
//connetion to database
 include_once('connection.php');
 session_start();
 if ( isset( $_SESSION['id'] ) ) {

$sql = "SELECT * from acc_coa";
$result = $conn->query($sql);
 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>COA4 detail</title>
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
 		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<style type="text/css">
		.table{
			width: 70%;	
		}
		.dtl{
				float: right;
			}
	</style>
 </head>
 <body>
 	<?php include_once('navbar.php') ?> 
	<div class="container">
	<div class="dtl">	<a href="coa.php"><button class="btn btn-danger"><-- GO BACK</button></a></div>
 <center><br><br>
  <table class="table table-bordered">
  	<thead class="thead "><h2>Chart Of Accounts Detail</h2></thead><br><br><br>
  	<tbody class="tbody">
  		<tr class="thead-dark">
  			<th>ID</th>
  			<th>COA1</th> 
  			<th>COA2</th>
  			<th>COA3</th>
  			<th>COA4</th>
  			<th>COA</th>
  		</tr>
  		<?php
			$i=1;
  			while($i<= $row = $result->fetch_assoc()){
  				$code=$row['coa4_acc_code'];

  				$sql1 = "SELECT * from acc_coa4 where acc_code = $code  ";
				$result1 = $conn->query($sql1);
				$row1 = $result1->fetch_assoc();

				$code1=$row1['coa3_acc_code'];
				$sql2 = "SELECT * from acc_coa3 where acc_code = $code1  ";
				$result2 = $conn->query($sql2);
				$row2 = $result2->fetch_assoc();

				$code2=$row2['coa2_acc_code'];
				$sql3 = "SELECT * from acc_coa2 where acc_code = $code2  ";
				$result3 = $conn->query($sql3);
				$row3 = $result3->fetch_assoc();

				$code3=$row3['coa1_acc_code'];
				$sql4 = "SELECT * from acc_coa1 where acc_code = $code3  ";
				$result4 = $conn->query($sql4);
				$row4 = $result4->fetch_assoc();

	echo "<tr>";
		echo     "<td>". $i."</td>";
		echo	"<td>".$row3['coa1_acc_code']."-".$row4['acc_desc']."</td>";
		echo	"<td>".$row2['coa2_acc_code'].'-'.$row3['acc_desc']."</td>";
		echo	"<td>".$row1['coa3_acc_code'].'-'.$row2['acc_desc']."</td>";
		echo	"<td>".$row['coa4_acc_code'].'-'.$row1['acc_desc']."</td>";
		echo	"<td>".$row['acc_code']." ".$row['acc_desc']."</td>";
		$i++;
		?>		
		<?php 
			echo "</tr>";
	
	
}
  	return ;	?>
  	</tbody>
  </table></center>
</div>
 </body>
 </html>
 <?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>