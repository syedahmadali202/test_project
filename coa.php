<?php
// database connection
	include_once('connection.php');
	session_start();
	if ( isset( $_SESSION['id'] ) ) {

	if (isset($_POST['save'])) {
		$coa4_acc_code =$_POST['coa4_acc_code'];
		$acc_code = $_POST['acc_code'];
		$acc_desc= $_POST['acc_desc'];
		
		//do some injection cleaning
		$coa4_acc_code = stripslashes($coa4_acc_code);
		$acc_code = stripslashes($acc_code);
		$acc_desc = stripslashes($acc_desc);
		
		$coa4_acc_code = strip_tags($coa4_acc_code);
		$acc_code = strip_tags($acc_code);
		$acc_desc = strip_tags($acc_desc);
		
		$coa4_acc_code = mysqli_real_escape_string($conn,$coa4_acc_code);
		$acc_code = mysqli_real_escape_string($conn,$acc_code);
		$acc_desc = mysqli_real_escape_string($conn,$acc_desc);

		$insert ="INSERT into acc_coa (coa4_acc_code,acc_code,acc_desc) values ('$coa4_acc_code','$acc_code','$acc_desc')";

		if ($conn->query($insert) === TRUE) {
    		
    		header("location:coa.php");
    		exit();
			}
			 else {
    		echo "Error: " . $insert . "<br>" . $conn->error;
			}
	}
	$sql = "SELECT * FROM acc_coa1";
	$result = $conn->query($sql);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Chart of Accounts</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<style type="text/css">
		<style type="text/css">
			
			label{
				font-size: 20px;
				font-weight: bold;
			}
			form{
				text-align: center;
				margin-top: 15%;
			}
			.btn{
				float: left;
				padding: 5px 19px;
			}
			.col-sm-6 {
				padding: 0px !important ;
			}
			.select2-container .select2-selection--single {
			    
			    height: 38px;
			    display: block;
			    }
			.dtl{
				float: right;
			}
		</style>
</head>
<body>
	<?php include_once('navbar.php') ?> 
		<div class="container">
							<div class="dtl">	<a href="coa-detail.php"><button class="btn btn-danger">View Detail</button></a></div>
							<br><br>
		<center><h2>CHART OF ACCOUNTS</h2></center>
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
	<form class="form-horizontal" method="post" enctype="multipart/formdata">
			
			<!-- coa1 detail -->
			<div class="form-group-">
				<div class="row">
					<label class="control-label col-sm-3"> Control Acc.  </label>
					<div class="col-sm-3">
					<input readonly="readonly" class="form-control" id="coa1_acc_code" type="text" name="coa1_acc_code" required="required" >
					</div>
					<div class="col-sm-6">
					<select class="js-example-placeholder-single js-states form-control" id="coa1_acc_desc" name="coa1_acc_desc" required="required">
	  					<option></option>
	  					<?php
	  						while($row = $result->fetch_assoc()){
							echo	"<option value=".$row['id'].">"  .$row['acc_desc']. "</option>";
						}
						?>
					</select>
				</div>
				</div>
			</div><br>
			<!-- coa2 detail -->
			<div class="form-group-">
				<div class="row">
					<label class="control-label col-sm-3"> Sub Acc.  </label>
					<div class="col-sm-3">
					<input readonly="readonly" class="form-control" id="coa2_acc_code" type="text" name="coa2_acc_code" required="required" >
					</div>
					<div class="col-sm-6">
					<select class="js-example-placeholder-single js-states form-control" id="coa2_acc_desc" name="coa2_acc_desc" required="required">
	  					<option></option>
					</select>
				</div>
				</div>
			</div><br>
			<!-- coa3 detail -->
			<div class="form-group-">
				<div class="row">
					<label class="control-label col-sm-3"> Sub Head  </label>
					<div class="col-sm-3">
					<input readonly="readonly"  class="form-control" id="coa3_acc_code" type="text" name="coa1_acc_code" required="required" >
					</div>
					<div class="col-sm-6">
					<select class="js-example-placeholder-single js-states form-control" id="coa3_acc_desc" name="coa3_acc_desc" required="required">
	  					<option></option>
					</select>
				</div>
				</div>
			</div><br>
				<!-- coa4 detail -->
			<div class="form-group-">
				<div class="row">
					<label class="control-label col-sm-3"> Acc. Code  </label>
					<div class="col-sm-3">
					<input readonly="readonly"  class="form-control" id="coa4_acc_code" type="text" name="coa4_acc_code" required="required" >
					</div>
					<div class="col-sm-6">
					<select class="js-example-placeholder-single js-states form-control" id="coa4_acc_desc" name="coa4_acc_desc" required="required">
	  					<option></option>
	  					

					</select>
				</div>
				</div>
			</div>
			<!-- applying javascript and ajax -->
			<script type="text/javascript">
				$("#coa1_acc_desc").on("change",function(){
					id = $(this).val();
					$.ajax({
					  url: "coa-ajax.php",
					  data: {
					    id:id,
					    func_name : 'acc_code'
					  },
					  success: function( result ) {
					  	result_get = result.split("///");
					    $( "#coa1_acc_code" ).val(result_get[0]);
					    // $( "#acc_code" ).val(result_get[1]);
					    $('#coa2_acc_desc').html("<option></option>");
					    $('#coa2_acc_code').val("");
					    $('#coa3_acc_desc').html("<option></option>");
					    $('#coa3_acc_code').val("");
					    $('#coa4_acc_desc').html("<option></option>");
					    $('#coa4_acc_code').val("");
					    $('#acc_desc').html("<option></option>");
					    $('#acc_code').val("");
						   	$.ajax({
							  url: "coa-ajax.php",
							  data: {
							    id_parent:result_get[0],
							    func_name : 'acc_code_child1'
							  },
							  dataType: 'json',
							  success: function( result ) {
							  	
							  	for(i=0;i<result.length;i++){
							  		result1 = result[i].split("___");
							  		$('#coa2_acc_desc').append('<option value="'+result1[1]+'">'+result1[0]+'</option>');
							  	} 	
							  }
							}); 
					  }
					});
				});
				$("#coa2_acc_desc").on("change",function(){
					id = $(this).val();
					$.ajax({
					  url: "coa-ajax.php",
					  data: {
					    id2:id,
					    func_name : 'acc_code1'
					  },
					   dataType: 'json',
					  success: function( result ) {
					  	ids = result[result.length-1];
						$('#coa3_acc_desc').html("<option></option>");
					    $('#coa3_acc_code').val("");
					    $('#coa4_acc_desc').html("<option></option>");
					    $('#coa4_acc_code').val("");
					    $('#acc_desc').html("<option></option>");
					    $('#acc_code').val("");
						$( "#coa2_acc_code" ).val(ids);
							  	for(i=0;i<result.length-1;i++){
							  		result1 = result[i].split("---");
							  		$('#coa3_acc_desc').append('<option value="'+result1[1]+'">'+result1[0]+'</option>')
							  	}
					    }
					});
				});
				$("#coa3_acc_desc").on("change",function(){
					id = $(this).val();
					$.ajax({
					  url: "coa-ajax.php",
					  data: {
					    id3:id,
					    func_name : 'acc_code2'
					  },
					   dataType: 'json',
					  success: function( result ) {
					  	ids = result[result.length-1];
						 $('#coa4_acc_desc').html("<option></option>");
					    $('#coa4_acc_code').val("");
					     $('#coa4_acc_desc').html("<option></option>");
					    $('#coa4_acc_code').val("");
					    $('#acc_desc').html("<option></option>");
					    $('#acc_code').val("");
						 $( "#coa3_acc_code" ).val(ids);
							  	for(i=0;i<result.length-1;i++){
							  		result1 = result[i].split("---");
							  		$('#coa4_acc_desc').append('<option value="'+result1[1]+'">'+result1[0]+'</option>')
							  	} 
					  }
					});
				});
				$("#coa4_acc_desc").on("change",function(){
					id = $(this).val();
					$.ajax({
					  url: "coa-ajax.php",
					  data: {
					    id4:id,
					    func_name : 'acc_code3'
					  },
					  success: function( result ) {
					  	 $('#acc_desc').html("<option></option>");
					    $('#acc_code').val("");
					  	result_get = result.split("///");
					    $( "#coa4_acc_code" ).val(result_get[0]);
					    $( "#acc_code" ).val(result_get[1]);
						 
					  }
					});
				});
					$(".js-example-placeholder-single").select2({
				    placeholder: "Select Account",
				    allowClear: true
				});

			</script>
			&nbsp &nbsp &nbsp 

			 <div class="form-group-">
					 	<div class="row">
			<label class="control-label col-sm-3">Ledger Code</label>
			<div class="col-sm-3">
			<input readonly="readonly"  class="form-control" id="acc_code" type="text" name="acc_code" required="required">
			</div>
			<div class="col-sm-6">
			<input class="form-control" placeholder="Put Account Description Here.." type="text" name="acc_desc" required="required">
		</div>
			</div>
		</div>
		</br>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-9">
					<input class="btn" type="submit" name="save" value="Save">
				</div>
			</div>
		</form>
		</div> 
		<div class="col-sm-3"></div>
	</div>
	</div>
</div>
	</div>
</body>
</html>
<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>