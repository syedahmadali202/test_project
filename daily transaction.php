<?php
include_once('connection.php');
session_start();
if ( isset( $_SESSION['id'] ) ) {
	//select data
	$sql = "SELECT * FROM acc_vou_type";
	$result = $conn->query($sql);
	if(isset($_POST['save'])){
		$dr = $_POST['dr'];
		$cr = $_POST['cr'];
		$dr1 ="";
		$cr1 = "";
		foreach( $dr as $key => $n ) {
			 @$dr1+= $n;
			 @$cr1+=$cr[$key];
		}
		if($dr1 == $cr1){
		 $vou_id= $_POST['vou_id'];
		 $vou_type= $_POST['vou_type'];
		 $vou_date= $_POST['vou_date'];
		 $vou_no= $_POST['vou_no'];
		 $vou_name= $_POST['vou_name'];
		 
		 	//do some injection cleaning
		$vou_id = stripslashes($vou_id);
		$vou_type = stripslashes($vou_type);
		$vou_date = stripslashes($vou_date);
		$vou_no = stripslashes($vou_no);
		$vou_name = stripslashes($vou_name);

		$vou_id = strip_tags($vou_id);
		$vou_type = strip_tags($vou_type);
		$vou_date = strip_tags($vou_date);
		$vou_no = strip_tags($vou_no);
		$vou_name = strip_tags($vou_name);
		
		$vou_id = mysqli_real_escape_string($conn,$vou_id);
		$vou_type = mysqli_real_escape_string($conn,$vou_type);
		$vou_date = mysqli_real_escape_string($conn,$vou_date);
		$vou_no = mysqli_real_escape_string($conn,$vou_no);
		$vou_name = mysqli_real_escape_string($conn,$vou_name);
		if($vou_name==""){
			$vou_name= '---';
		}
		 $insert1="INSERT into acc_vou_mst (vou_type_id,vou_date,vou_no,vou_narration) values ('$vou_type','$vou_date','$vou_no','$vou_name')";
		$conn->query($insert1);
		$coa_code = $_POST['coa_code'];
		$dr = $_POST['dr'];
		$cr = $_POST['cr'];
		$cheque_no = $_POST['cheque_no'];
		$number = $_POST['number'];
		$remarks = $_POST['remarks'];	

		foreach( $coa_code as $key => $n ) {
		  // print "The name is ".$n." and coa_code is ".$vou_id.", thank you\n";
			//do some injection cleaning
		$n = stripslashes($n);
		$dr[$key] = stripslashes($dr[$key]);
		$cr[$key] = stripslashes($cr[$key]);
		$cheque_no[$key] = stripslashes($cheque_no[$key]);
		$number[$key] = stripslashes($number[$key]);
		$remarks[$key] = stripslashes($remarks[$key]);
		
		$n = strip_tags($n);
		$dr[$key] = strip_tags($dr[$key]);
		$cr[$key] = strip_tags($cr[$key]);
		$cheque_no[$key] = strip_tags($cheque_no[$key]);
		$number[$key] = strip_tags($number[$key]);
		$remarks[$key] = strip_tags($remarks[$key]);
		
		$n = mysqli_real_escape_string($conn,$n);
		$dr[$key] = mysqli_real_escape_string($conn,$dr[$key]);
		$cr[$key] = mysqli_real_escape_string($conn,$cr[$key]);
		$cheque_no[$key] = mysqli_real_escape_string($conn,$cheque_no[$key]);
		$number[$key] = mysqli_real_escape_string($conn,$number[$key]);
		$remarks[$key] = mysqli_real_escape_string($conn,$remarks[$key]);
		if($number[$key]==""){
			$number[$key]= '---';
		}
		if($remarks[$key]==""){
			$remarks[$key]='---';
		}
		 $insert ="INSERT into acc_vou_dtl (vou_id,acc_code,dr,cr,transaction_type,reference_no,remarks) 
		 values ('$vou_id','$n','$dr[$key]','$cr[$key]','$cheque_no[$key]','$number[$key]','$remarks[$key]')";
		 $conn->query($insert);
		}
		header('location: daily transaction.php');
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Daily Transaction</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<style type="text/css">
		select{
			width: 100%;
		}
		.container li {
		    list-style: none;
		    float: left;
		    padding: 5px;
		}
		.col-sm-11{
			padding: 0px;
		}
		.pull-right{
			float: right;
		}
		span.select2-selection.select2-selection--single {
		    height: 38px;
		}
		.col-sm-11 {
			padding: 0px !important;
		}
		.select2 select2-container select2-container--default{
			width: 100 px !important;
		}
		</style>
</head>
<body>
	<?php include_once('navbar.php') ?> 
	<br><br>
	<center><h3>Daily Transaction</h3></center><br><br>
	<form class="form" method="post" action="">
<div class="container">
					<div class="row">
							<li>Voucher ID</li>
							<li><input class="form-control" readonly="readonly" required="required"  type="text" name="vou_id"
								 value="<?php
								 	$sql1 = "SELECT * FROM acc_vou_mst ORDER BY id DESC";
									$result1 = $conn->query($sql1);
									$row1 = $result1->fetch_assoc();	
									echo @$sum = $row1['id']+1;
									 ?>"
								/></li>
							<li>Voucher Type</li>
							<li>
								<select   required="required"  class="js-example-placeholder-single1 js-states form-control" id="vou_type" name="vou_type">
				  					<option></option>
				  					<?php
				  						while($row = $result->fetch_assoc()){
										echo	"<option value=".$row['id'].">"  .$row['vou_name']. "</option>";
									}
					
									?>
					</select>
							</li>
							<li>Dated</li>
							<li><input required="required" class="form-control" type="date" id="theDate" type="text" name="vou_date"></li>
							<li>Voucher #</li>
							<li><input readonly="readonly" required="required" class="form-control" id="vou_no" type="text" name="vou_no"></li>
					</div><br>
					<div class="row">
						<div class="col-sm-1">Voucher Narration</div>
						<div class="col-sm-11 "><input  class="form-control" type="text" name="vou_name" placeholder="Voucher Name"></div>
					</div>
			<br><br>
	<div class="row">
		<table class="table table-bordered">
			<tbody class="tbody" id="get_data">
				<tr class="thead-dark">
					<th>Account Title</th>
					<th>DR Amount</th>
					<th>CR Amount</th>
					<th>Type</th>
					<th>Number</th>
					<th>Remarks</th>
				</tr>
				<?php
					for ($i=0; $i < 2; $i++) { ?>
						<tr>
					<td>
						<select class="coa_code js-example-placeholder-single js-states form-control" required="required"  name="coa_code[]" >
							<option></option>
							<?php
							$sql = "SELECT * FROM acc_coa";
							$result = $conn->query($sql);
							while($row = $result->fetch_assoc()){
										echo	"<option value=".$row['acc_code'].">"  .$row['acc_desc']. "</option>";
									}
							?>
						</select>
					</td>
					<td><input required="required" value="0" type="text" name="dr[]" class="dr"></td>
					<td><input required="required" value="0" type="text" name="cr[]" class="cr"></td>
					<td>
						<select name="cheque_no[]">
							<option value="---">---</option>
							<option value="cheque no">Cheque No</option>
							<option value="cash">Cash No</option>

						</select>
					</td>
					<td><input  type="text" name="number[]"></td>
					<td><input  type="text" name="remarks[]"></td>
				</tr>
				<?php
					}
				?>
			</tbody>
		</table>	
	</div>
	<div class="pull-right">
			<input type="button" class="btn btn-primary" name="add" id="add" value="Add">
		</div>
			<div class="pull-left">
				<input type="submit" class="btn " name="save" id="save" value="Save">
			</div>
</div>
</form>
</body>
</html>
		<!-- applying javascript and ajax -->
			<script type="text/javascript">
				var date = new Date();
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				if (month < 10) month = "0" + month;
				if (day < 10) day = "0" + day;
				var today = year + "-" + month + "-" + day;
				document.getElementById('theDate').value = today;

				$("#save").on("click",function(e){
					length = $('.coa_code').length;
					sum=0;
					sum1=0;
					for(i=0;i<length;i++){
						val = $('.dr').eq(i).val();
						sum = +sum + +val;
						val1 = $('.cr').eq(i).val();
						sum1 = +sum1 + +val1;
					}
					if(sum1 == sum){
					}
					else{
						e.preventDefault();
						alert('Your DR amount not Equal to CR amount');
					}
				});
				$("#vou_type").on("change",function(){
					id = $(this).val();
					$.ajax({
					  url: "transaction-ajax.php",
					  data: {
					    id:id,
					    func_name : 'acc_code'
					  },
					  success: function( result ) {
					  	result_get = result.split("///");
					    $( "#vou_id" ).val(result_get[0]);
					    $( ".vou_id" ).val(result_get[0]);
					    $( "#vou_no" ).val(result_get[1]);
					  }
					});
				});
				$("#add").on("click",function(){
					val = $("#vou_id").val();
					$('#get_data').append('<tr> <td> <select required="required" class="coa_code js-example-placeholder-single2 js-states form-control"  name="coa_code[]"> <option></option> <?php $sql = "SELECT * FROM acc_coa"; $result = $conn->query($sql); while($row = $result->fetch_assoc()){ echo "<option value=".$row['acc_code'].">" .$row['acc_desc']. "</option>"; } ?> </select> </td> <td><input type="text" required="required" value="0" name="dr[]" class="dr"></td> <td><input type="text" value="0" name="cr[]" required="required" class="cr"></td> <td> <select name="cheque_no[]"><option value="---">---</option> <option value="cheque no">Cheque No</option><option value="cash">Cash No</option> </select> </td> <td><input type="text" name="number[]"></td> <td><input type="text" name="remarks[]"></td> </tr>');
				$(".js-example-placeholder-single2").select2({
				    placeholder: "Select Account",
				    allowClear: true
				});
				});
				$(".js-example-placeholder-single").select2({
				    placeholder: "Select Account",
				    allowClear: true
				});
				$(".js-example-placeholder-single1").select2({
				    placeholder: "Select Voucher Type",
				    allowClear: true
				});
				$(".js-example-placeholder-single3").select2({
				    placeholder: "Select Transaction",
				    allowClear: true
				});
			</script>
			<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>