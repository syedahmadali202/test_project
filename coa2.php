<?php
// database connection
	include_once('connection.php');
	session_start();
	if ( isset( $_SESSION['id'] ) ) {

	$sql = "SELECT * FROM acc_coa1";
	$result = $conn->query($sql);
// inserting form data
	if (isset($_POST['submit'])) {
		$coa1_acc_code =$_POST['coa1_acc_code'];
		$acc_code = $_POST['acc_code'];
		$acc_desc= $_POST['acc_desc'];
		$coa1_acc_desc= $_POST['coa1_acc_desc'];

			//do some injection cleaning
		$coa1_acc_code = stripslashes($coa1_acc_code);
		$acc_code = stripslashes($acc_code);
		$acc_desc = stripslashes($acc_desc);
		$coa1_acc_desc = stripslashes($coa1_acc_desc);

		$coa1_acc_code = strip_tags($coa1_acc_code);
		$acc_code = strip_tags($acc_code);
		$acc_desc = strip_tags($acc_desc);
		$coa1_acc_desc = strip_tags($coa1_acc_desc);



		$coa1_acc_code = mysqli_real_escape_string($conn,$coa1_acc_code);
		$acc_code = mysqli_real_escape_string($conn,$acc_code);
		$acc_desc = mysqli_real_escape_string($conn,$acc_desc);
		$coa1_acc_desc = mysqli_real_escape_string($conn,$coa1_acc_desc);

		$insert ="INSERT into acc_coa2 (coa1_acc_code,coa1_acc_desc,acc_code,acc_desc) values ('$coa1_acc_code','$coa1_acc_desc','$acc_code','$acc_desc')";

		if ($conn->query($insert) === TRUE) {
    		echo "New record created successfully";
    		header( 'location:coa2.php');
			}
			 else {
    		echo "Error: " . $insert . "<br>" . $conn->error;
			}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Sub Control Accounts</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

	<style type="text/css">
		<style type="text/css">
			
			label{
				font-size: 20px;
				font-weight: bold;
			}
			form{
				text-align: center;
				margin-top: 15%;
			}
			.btn{
				float: left;
				padding: 5px 19px;
			}
			.col-sm-6 {
				padding: 0px !important ;
			}
			.select2-container .select2-selection--single {
			    
			    height: 35px;
			    display: block;
			    }
			 .dtl{
				float: right;
			}
		</style>

</head>
<body>
	<?php include_once('navbar.php') ?> 
		<div class="container">
							<div class="dtl">	<a href="coa2-detail.php"><button class="btn btn-danger">View Detail</button></a></div>
							<br><br>
		<center><h2>Sub Control Accounts</h2></center>
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">

	<form class="form-horizontal" method="post" enctype="multipart/formdata">
			
			<div class="form-group-">
				<div class="row">
					<label class="control-label col-sm-3"> Control Head  </label>
					<div class="col-sm-3">
					<input readonly="readonly"  class="form-control" id="coa1_acc_code" type="text" name="coa1_acc_code" required="required" >
					</div>
					<div class="col-sm-6">
					<select  class="js-example-placeholder-single js-states form-control" id="coa1_acc_desc" name="coa1_acc_desc">
	  						<option></option>
	  					<?php

	  						while($row = $result->fetch_assoc()){

							echo	"<option value=".$row['id'].">"  .$row['acc_desc']. "</option>";
						}
		
						?>

					</select>
				</div>
				</div>
			</div>
			
			<!-- applying javascript and ajax -->
			<script type="text/javascript">
				$("#coa1_acc_desc").on("change",function(){
					id = $(this).val();
					$.ajax({
					  url: "coa2_ajax.php",
					  data: {
					    id:id,
					    func_name : 'acc_code'
					  },
					  success: function( result ) {
					  	result_get = result.split("///");
					    $( "#coa1_acc_code" ).val(result_get[0]);
					    $( "#acc_code" ).val(result_get[1]);
					  }
					});
				});

				$(".js-example-placeholder-single").select2({
				    placeholder: "Select Account",
				    allowClear: true
				});

			</script>
			&nbsp &nbsp &nbsp 

			 <div class="form-group-">
					 	<div class="row">
			<label class="control-label col-sm-3">Sub Control</label>
			<div class="col-sm-3">
			<input readonly="readonly"  class="form-control" id="acc_code" type="text" name="acc_code" required="required">
			</div>
			<div class="col-sm-6">
			<input class="form-control" placeholder="Put Account Description Here.." type="text" name="acc_desc" required="required">
		</div>
			</div>
		</div>
		</br>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-9">
					<input class="btn" type="submit" name="submit" value="Save">
				</div>
			</div>



		</form>
		</div> 
		<div class="col-sm-3"></div>
	</div>
	</div>
</div>
	</div>

</body>
</html>
<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>