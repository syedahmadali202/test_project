<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  
</head>
<body>

<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Accounts</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="coa1.php">COA1</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="coa2.php">COA2</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="coa3.php">COA3</a>
      </li>    
       <li class="nav-item">
        <a class="nav-link" href="coa4.php">COA4</a>
      </li>    
       <li class="nav-item">
        <a class="nav-link" href="coa.php">COA</a>
      </li>    
       <li class="nav-item">
        <a class="nav-link" href="voucher_type.php">Voucher Type</a>
      </li>    
       <li class="nav-item">
        <a class="nav-link" href="daily transaction.php">Daily Transaction</a>
      </li>    
       <li class="nav-item">
        <a class="nav-link" href="ledger.php">Ledger</a>
      </li>    

      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        More Detail
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="balance.php">Balance Sheet</a>
        <a class="dropdown-item" href="income.php">Income Statement</a>
        <a class="dropdown-item" href="notes.php">Notes to the Account(A)</a>
        <a class="dropdown-item" href="notes1.php">Notes to the Account(B)</a>
        <a class="dropdown-item" href="trial.php">Trial Balance</a>
        <a class="dropdown-item" href="bank.php">Bank Scroll</a>
        <a class="dropdown-item" href="cash.php">Cash Scroll</a>
        <a class="dropdown-item" href="daily.php">Daily Scroll</a>
        <a class="dropdown-item" href="payment.php">Cash Payment</a>
      </div>
    </li> 
     </li>    
       <li class="nav-item">
        <a class="nav-link" href="logout.php">Logout</a>
      </li> 
    </ul>
  </div>  
</nav>
<br>

</body>
</html>


