<?php include_once('connection.php');?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<style type="text/css">
		body{
			background-image: url('images/1.jpg');
		}
		.link1{
			color: black;
			font-weight: bold;
		}
		.btn: hover{
			opacity: 0.7;
		}
		.link2{
			float: right;
			color: black;
			font-weight: bold;
		}
		p{
			text-align: center;
		}
	</style>
</head>
<body>
	<br><br>
	<center><h3>Request Password Reset</h3></center><br><br>
	<div class="container">
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<form method="post">
					<input class="form-control" type="text" name="email" placeholder="Email.."><br>
					<input class="btn btn-success btn-block" type="submit" name="submit" value="Request Password Reset"><br>
					<p>Remembered your password? <a class="link1" href="index.php"> Login here</a></p>
				</form>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</div>
</body>
</html>
