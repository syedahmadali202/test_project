<?php
include_once('connection.php');
session_start();
if ( isset( $_SESSION['id'] ) ) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cash Scroll</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<style type="text/css">
	.row1{
		margin-top: 15%;
		font-size: 16px;
		font-weight: bold;
		margin-bottom: 5%;
	}
	button{
		float: right;
	}
	@media print {
   .footer{
   position: relative;
   bottom:0;
	}
   #btn1{
   	display: none;
   }
   #btn{
   	display: none;
   }
	}
</style>
<script>
function myFunction() {
  window.print();
}
</script>
<body>
	<?php include_once('navbar.php'); ?>
	<br><br>
	<center><h2>Cash Scroll</h2></center><br>
	<div class="container" id="btn1">
		<div class="row ">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<form method="post">
					<div class="row ">
						<div class="col-sm-2"><label class="label-control">Date: </label></div>
						<div class="col-sm-10"><input class="form-control" id="theDate" type="date" name="date"></div>
					</div><br>
					<div class="row">
						<div class="col-sm-2"></div>
						<div class="col-sm-10"><input class="btn"  type="submit" name="submit" value="View Report"></div>
					</div>
				</form>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</div>
	<div class="container">
		<?php if(isset($_POST['submit'])){
	 $date = $_POST['date'];
	 $dr1=0;
	 $cr1=0;
	  ?>
		<button type="btn" onclick="myFunction()" id="btn" class="btn btn-danger" value="Print"> Print Report</button>
	</br></br>
	
		<table class="table table-bordered" id="content" >
			<tbody>
				<tr class="thead-dark" >
					<th>Voucher#</th>
					<th>COA</th>
					<th>Cheque Type / NO</th>
					<th>Receipt</th>
					<th>Payment</th>
					<th>Signature</th>
				</tr>
				<tr>
					<td>---</td>
					<td>Opening Cash in Hand</td>
					<td>---</td>
					<?php $select4 ="select * from acc_coa where acc_desc = 'CASH IN HAND'";
					$result4 = $conn->query($select4);
					$row5 = $result4->fetch_assoc();
					$acc_code1 = $row5['acc_code']; 

					$select5 ="select * from acc_vou_dtl where acc_code = '$acc_code1' ";
					$result5 = $conn->query($select5);
					while ($row6 = $result5->fetch_assoc()) {
						$dr1+= $row6['dr'];
						$cr1+= $row6['cr'];
					}
					$balance = $dr1-$cr1;
					echo '<td>'.$balance.'</td>'
					 ?>
					<td>---</td>
					<td></td>
				</tr>
				<?php
				$select= " SELECT *  from acc_vou_mst where vou_date = '$date' AND (vou_type_id = 7 OR vou_type_id = 8)  order by vou_type_id ";
				$result = $conn->query($select);
				$temp = 0;
				$i=0;
				$dr=0;
				$cr=0;
				while($row = $result->fetch_assoc()){
					$id=$row['id'];
					$select1 = "SELECT * from acc_vou_dtl where vou_id ='$id'";
					$result1 = $conn->query($select1);
					$vo_id = $row['vou_type_id'];
					$i++;
					if($temp != $row['vou_type_id'] && $i>1){
						echo '<tr>';
						echo '<td>---</td>';
						echo '<td>---</td>';
						echo '<td>---</td>';
						echo '<td>---</td>';
						echo '<td>---</td>';
						echo '<td>---</td>';
						echo '</tr>';
					}
					$temp = $row['vou_type_id'];
					while($row2 =$result1->fetch_assoc()){
					$vo_id = $row['vou_type_id'];
					$select2 = "select * from acc_vou_type where id = '$vo_id'";
					$result2 = $conn->query($select2);
					$row3 = $result2->fetch_assoc();

					echo '<tr>';					
					echo '<td>'.$row3['vou_abrv'].' '.$row['vou_no'] .'</td>';
					$acc_code =$row2['acc_code'];
					$select3 = "select * from acc_coa where acc_code = '$acc_code'";
					$result3 = $conn->query($select3);
					$row4 = $result3->fetch_assoc();
					echo '<td>'. $row4['acc_desc']. '</td>';

					if($row2['transaction_type'] == 'cheque no'){
					echo '<td>'.$row2["transaction_type"].' '.$row2["reference_no"] . '</td>';
					}
					else{
						echo '<td>---</td>';
					}
					echo '<td>'.$row2['dr'] . '</td>';
					echo '<td>'.$row2['cr'] . '</td>';
					echo '<td> </td>';
					echo '</tr>';
					@$dr+= $row2['dr'];
					@$cr+= $row2['cr'];
					}		
				}?>
			</tbody>
		</table><br>
		<div class="row">
				<div class="col-sm-7"></div>
				<div class="col-sm-5">
					<div class="row">
						<div class="col-sm-6">
							<h6>Opening</h6>
							<h6>Total Receipt</h6>
							<h6>Total</h6>
							<h6>Total Paynment</h6>
							<h6>Cash in Hand</h6>
						</div>
						<div class="col-sm-6">
							<h6><?php echo $balance?></h6>
							<h6><?php echo $dr; ?></h6>
							<h6><?php echo $balance+$dr; ?></h6>
							<h6><?php echo $cr; ?></h6>
							<h6><?php echo $balance+$dr-$cr; ?></h6>
						</div>
					</div>
				</div>
			</div>
		<div class="row row1 footer" style="bottom: 0;" >
			<div class="col-sm-4">Accountant</div>
			<div class="col-sm-4">Accounts Manager</div>
			<div class="col-sm-4">Chief Executive</div>
		</div>
		<?php }?>
	</div>
</body>
</html>
<script type="text/javascript">
			var date = new Date();
			var day = date.getDate();
			var month = date.getMonth() + 1;
			var year = date.getFullYear();
			if (month < 10) month = "0" + month;
			if (day < 10) day = "0" + day;
			var today = year + "-" + month + "-" + day;
			document.getElementById('theDate').value = today;
</script>
<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>