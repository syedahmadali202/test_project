<?php
include_once('connection.php');
session_start();
if ( isset( $_SESSION['id'] ) ) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cash Paynment</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</head>
<style type="text/css">
span.select2-selection.select2-selection--single {
		    height: 38px;
		 
		}
	.row1{
		margin-top: 15%;
		font-size: 16px;
		font-weight: bold;
		margin-bottom: 5%;
	}
	button{
		float: right;
	}
	.footer{
	position: relative;
	bottom: 0;
	}
	@media print {
	 
   .footer{
     
   position: relative;
   bottom:0;
	}
	
   #btn{
   	display: none;
   }
    #btn1{
   	display: none;
   }
	}
	.row1{
		margin: 0px !important;
	}
	.footer table tr th{
		border-top:0px;
	}
	.footer table tr td{
		height: 150px;
	}
	
</style>
<script type="text/javascript">


function myFunction() {
	var today = new Date();
				var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
				var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
				var dateTime = date+'  '+time;
	$('#print_date').html(dateTime);
  window.print();
  

}

</script>
<body>
	<?php include_once('navbar.php'); ?>
	<br><br>
<!-- 	<center><h2>Voucher</h2></center> -->
	<div class="container">
		<div class="row " id="btn1">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<form method="post">
					<div class="row ">
						<div class="col-sm-3"><label class="label-control">Voucher: </label></div>
						<div class="col-sm-9">
							<select name="voucher_id"  required="required"  class="js-example-placeholder-single js-states form-control">
								<option></option>
								<?php 
									$select4 = "select* from acc_vou_mst"; 
									$result4 = $conn->query($select4);
									while ($row5 = $result4->fetch_assoc() ) {
										echo	"<option value=".$row5['id'].">"  .$row5['id']. "</option>";
									}
								?>

							</select>
						</div>
					</div><br>
					<div class="row">
						<div class="col-sm-3"></div>
						<div class="col-sm-9"><input class="btn" id="view"  type="submit" name="submit" value="View Report"></div>
					</div>
				</form>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</div>
	<div class="container">
		
		<?php if(isset($_POST['submit'])){ 
		  $voucher_id= $_POST['voucher_id'];
		  $select= " SELECT *  from acc_vou_mst WHERE  id = '$voucher_id'  order by id ";
				$result = $conn->query($select);
				$temp = 0;
				$i=0;
				$dr=0;
				$cr=0;
				$row = $result->fetch_assoc();
					$id=$row['id'];
					$vo_id = $row['vou_type_id'];
					$vo_date = $row['vou_date'];
		?><br><br>
		<button type="btn" onclick="myFunction()" id="btn" class="btn btn-danger" value="Print"> Print Report</button>
			<center><h3><?php 
					$select2 = "select * from acc_vou_type where id = '$vo_id'";
					$result2 = $conn->query($select2);
					$row3 = $result2->fetch_assoc();
					echo $row3['vou_name'];

		 ?></h3></center>
		</br></br>

		<div class="row">
			<div class="col-sm-2"><b>Voucher Date:</b></div>
			<div class="col-sm-2"><?php echo date("Y-m-d"); ?></div>
			<div class="col-sm-2"><b>Entry Date:</b></div>
			<div class="col-sm-2"><?php echo $vo_date; ?></div>
			<div class="col-sm-2"><b>Voucher#:</b></div>
			<div class="col-sm-2"><?php echo $id; ?></div>
		</div><br>
		<div class="row">
			<div class="col-sm-2"><b>Print Date & Time:</b></div>
			<div class="col-sm-6" id="print_date"></div>
			<div class="col-sm-3"><b>Voucher Prepared by:</b></div>
			<div class="col-sm-1"></div>
		</div>

	</br></br>
		
		<table class="table table-bordered" id="content" >
			<tbody>
				<tr class="thead-dark" >
					<th>Code</th>
					<th>Title</th>
					<th>Transaction Type / No</th>
					<th>Dr</th>
					<th>Cr</th> 	
				</tr>

				<?php
				

					$select1 = "SELECT * from acc_vou_dtl where vou_id ='$id'";
					$result1 = $conn->query($select1);
					
					while($row2 =$result1->fetch_assoc()){
					
					echo '<tr>';
					
					echo '<td>'.$row2['acc_code'] .'</td>';

					$acc_code =$row2['acc_code'];
					$select3 = "select * from acc_coa where acc_code = '$acc_code'";
					$result3 = $conn->query($select3);
					$row4 = $result3->fetch_assoc();

					echo '<td>'. $row4['acc_desc']. '</td>';

					if($row2['transaction_type'] != ''){

				
					echo '<td>'.$row2["transaction_type"].' '.$row2["reference_no"] . '</td>';
					}
					else{
						echo '<td>---</td>';
					}
					
					echo '<td>'.$row2['dr'] . '</td>';
					echo '<td>'.$row2['cr'] . '</td>';
					echo '</tr>';
					@$dr+= $row2['dr'];
					@$cr+= $row2['cr'];
					}		
					
				?>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<th>Total: <?php echo $dr; ?></th>
						<th>Total: <?php echo $cr; ?></th>
					</tr>

			</tbody>
		</table>
	
		<div class="row footer" style="margin-top: 15%;" >
			<table class="table">

				<tr >
					<th>Checked</th>
					<th>Audited</th>
					<th>Approved</th>
					<th>Received</th>
				</tr>
				<tr class="table table-bordered">
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
		<?php } ?>
	</div>
</body>
</html>
<script type="text/javascript">
		$(".js-example-placeholder-single").select2({
				    placeholder: "Select Voucher",
				    allowClear: true
				});
// 		$('#print_r').click(function(){
// var today = new Date();
// 				var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
// 				var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
// 				var dateTime = date+'  '+time;
// 	$('#print_date').html(dateTime);
// });

</script>
<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>