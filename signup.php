<?php 
	include_once('connection.php');
	session_start();
	
	if(isset($_POST['submit'])){

		$name =	$_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		//injections
		$name = stripslashes($name);
		$email = stripslashes($email);
		$password = stripslashes($password);

		$name = strip_tags($name);
		$email = strip_tags($email);
		$password = strip_tags($password);

	 	$name = mysqli_real_escape_string($conn,$name);
		$email = mysqli_real_escape_string($conn,$email);
		$password = mysqli_real_escape_string($conn,$password);


//insert data 
			$query = "INSERT into signup (name,email,password) values ('$name','$email','$password')";
	
				if ($conn->query($query) === TRUE) {
    		echo "New record created successfully";
    		 header( 'location:index.php');
			}
			 else {
    		echo "Error: " . $query . "<br>" . $conn->error;
			}

	}

 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Signup</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<style type="text/css">
		body{
			background-image: url('images/1.jpg');
		}
		h6{
			color: black;
		}
		.btn: hover{
			opacity: 0.7;
		}

	</style>
</head>
<body>
	<br><br>
	<center><h3>Register now to create Account</h3></center><br><br>
	<div class="container">
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<form method="post">
					<input class="form-control" required="required" type="text" name="name" placeholder="User Name.."><br>
					<input class="form-control" required="required" type="text" name="email" placeholder="Email.."><br>
					<input class="form-control" required="required" type="password" name="password" placeholder="Password.."><br>
					<input class="btn btn-success btn-block" type="submit" name="submit" value="Register"><br>
					<center><a href="index.php"><h6>Already have an account?</h6></a></center>
				</form>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</div>
</body>
</html>