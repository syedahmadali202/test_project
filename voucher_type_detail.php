<?php include_once('connection.php');
session_start();
if ( isset( $_SESSION['id'] ) ) {
$sql = "SELECT * FROM acc_vou_type";
$result = $conn->query($sql);
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>VOUCHER TYPE DETAIL</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<style type="text/css">
		.table{
			width: 50%;	
		}
		.dtl{
				float: right;
			}
	</style>
</head>
<body>
	<?php include_once('navbar.php') ?> 
	<div class="container">
	<div class="dtl">	<a href="voucher_type.php"><button class="btn btn-danger"><-- GO BACK</button></a></div>
	<center><br><br>
  <table class="table table-bordered">
  	<thead class="thead "><h2>Vouchet Type Detail</h2></thead><br><br><br>
  	<tbody class="tbody">
  		<tr class="thead-dark">
  			<th>ID</th>
  			<th>Abrv</th>
  			<th>Name</th>
  		</tr>
  		<?php
  		$i=1;
  			while($i<=$row = $result->fetch_assoc()){
	
	echo "<tr>";
		if($i<10){
		echo     "<td>0". $i."</td>";
		echo	"<td>".$row['vou_abrv']."</td>";
		echo	"<td>".$row['vou_name']."</td>";
	}
	else{
		echo     "<td>". $i."</td>";
		echo	"<td>".$row['vou_abrv']."</td>";
		echo	"<td>".$row['vou_name']."</td>";
	}
		$i++;
		?>	
		<?php 
	echo "</tr>";
}
  		?>
  	</tbody>
  	
  </table></center>
</div>
</body>
</html>
<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>