<?php
include_once('connection.php');
session_start();
if ( isset( $_SESSION['id'] ) ) {

$sql = "SELECT * from acc_coa";
$result = $conn->query($sql);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Trial Balance</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
	<?php include_once('navbar.php'); ?>
	<br><br>
	<center><h2>Trial Balance</h2></center>

	<div class="container">
		<center><br><br>
  <table class="table table-bordered">
  	<tbody class="tbody">
  		<tr class="thead-dark">
  			<th>COA</th>
  			<th>Opening</th> 
  			<th>Debit</th>
  			<th>Credit</th>
  			<th>Balance</th>
  		</tr>
  		<?php

		
		$slct="SELECT vou_type_id,acc_code,SUM(dr) DR,SUM(cr) CR,SUM(DR-CR) BALANCE
		FROM acc_vou_dtl,acc_vou_mst
		WHERE acc_vou_mst.id = acc_vou_dtl.vou_id
		GROUP BY vou_date,acc_code";
		$result2 = $conn->query($slct);

		

  		$dr8="";
  		$cr8="";
  		$Balance8="";
		

  			while($row9 = $result2->fetch_assoc()){
  		// 		$acc_code=$row['acc_code'];
				// $select= "SELECT * FROM  acc_vou_dtl where acc_code like '$acc_code%' ";
				// $result1 = $conn->query($select);
				//  $dr8="";
				// $cr8="";
				// while($row8 = $result1->fetch_assoc()){
				// 	 @$dr8 += $row8['dr'];
				// 	 @$cr8 += $row8['cr'];
				// }

		
	echo "<tr>";
	$code=$row9['acc_code'];
		$slect= "select *from acc_coa where acc_code='$code'";
		$result3 = $conn->query($slect);
		$row10 = $result3->fetch_assoc();

		echo     "<td>". $row10['acc_desc']."</td>";
		if($row9['vou_type_id']==6){ 
			echo	"<td>Opening Vou</td>";
		}
		else{
			echo	"<td>---</td>";
		}
		
		echo	"<td>".$row9['DR']."</td>";
		echo	"<td>".$row9['CR']."</td>";
		$Balance = $row9['BALANCE'];
		if($Balance<0){
			$Balnce= $Balance*-1;
			echo	"<td>Cr.&nbsp&nbsp&nbsp".$Balnce."</td>";
			}
			else if($Balance>0){
			echo	"<td>Dr.&nbsp&nbsp&nbsp".$Balance."</td>";
			}

			else{
				echo	"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".$Balance."</td>";
			}
			@$dr8 += $row9['DR'];
			@$cr8 += $row9['CR'];
			@$Balance8+=$Balance;
	echo "</tr>";
	
	
}
  		?>
  		<tr>
  			<td></td>
  			<td></td>
  			<td><h6>Total= <?php echo $dr8; ?></h6></td>
  			<td><h6>Total= <?php echo $cr8; ?></h6></td>
  			<td><h6>Total= <?php echo $Balance8; ?></h6></td>
  		</tr>
  	</tbody>
  	
  </table></center>
	</div>
</body>
</html>
<?php
} else {
    // Redirect them to the login page
    header("Location: index.php");
}
?>